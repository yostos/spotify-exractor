# Spotify Exractor

## About

Spotipy を使って Spotify APIからプレイリストから収録している曲の属性情報をCSVでダウンロードしてくるサンプルコードです。Spotify APIにPythonでアクセスするためのライブラリ[Spotipy](https://spotipy.readthedocs.io/en/2.16.1/#)を参考にしています。

[Spotify API Document](https://developer.spotify.com/documentation/web-api)を読むとわかりますが、Spotify APIを使うとSpotifyが曲ごとに保持しているいろんな情報をプログラムで取得できます。

例えば、以下はこのサンプルコードを使って、70年代と2000年代のヒットソングのプレイリストから300曲程度の情報を取得して、EXCELでBPMのばらつきをグラフ化して確認したものです。

![BPMの年代ごとのばらつき](./docs/images/sample.webp)




## Installation

試してみたい方は、以下の依存するパッケージをインストールして実行ください。


Spotipyは多数のパッケージに依存しているので、Pyenvやasdfなどローカルで独自のPython環境が作れるツールを併用してローカルな環境で実行することをオススメします。

```sh
# 依存パッケージのインストール
$ pip install spotipy, pandas

# .env ファイルを新規に作成して、以下の形式でSPOTIFY APIの認証情報を記入してください。
#    SPOTIFY_CLIENT_ID="x33e775axxxxxxxxxxxxxxxxxxxxxxxx"
#    SPOTIFY_CLIENT_SECRET="41ff9a7e289xxxxxxxxxxxxxxxxxxxxx"
$ echo 'SPOTIFY_CLIENT_ID="x33e775axxxxxxxxxxxxxxxxxxxxxxxx"' >.env
$ echo 'SPOTIFY_CLIENT_SECRET="41ff9a7e289xxxxxxxxxxxxxxxxxxxxx"' >>.env
```

## Execution

サンプルコードを呼び出すシェルスクリプトは、冒頭のグラフを作るためにSpotifyの "[All out 2000s](https://open.spotify.com/playlist/37i9dQZF1DX4o1oenSJRJd?si=81c0989289294480)"と"[All out 70s](https://open.spotify.com/playlist/37i9dQZF1DWTJ7xPn4vNaz?si=a0b2d96908ca4dfa)"のプレイリストから曲の属性情報を取得してCSVで書き出します。

自身で別のプレイリストを使いたい場合は直接Pythonから呼び出して引数にPlaylist IDを指定してください。


```sh
# 以下のコマンドでSpotifyから ”All Out 2000s" と "All Out 70s"のプレイリスト
# から情報を取得してCSVに書き出します。
$ ./export_spotify.sh

# 自身で好きなSpotifyのプレイリストを指定したい場合は、以下のようにPlaylist
# IDを直接していしてください。Plalist IDはSpotifyのプレイリストをブラウザで開いた
# 時のURLの末尾です。 XXXXXXXXXXXXXXXXXXXXXX の部分がPlaylist IDです。
$ python --playlist XXXXXXXXXXXXXXXXXXXXXXX XXXXXXXXXXXXXXXXXXXXXX XXXXXXXXXXXXXXXXXXX

```
