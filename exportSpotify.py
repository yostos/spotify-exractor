import pandas as pd
import spotipy
from spotipy.oauth2 import SpotifyClientCredentials
import time
import os
import sys
import argparse
from dotenv import load_dotenv

# Spotify API credentials from .env
load_dotenv()
client_id = os.environ["SPOTIFY_CLIENT_ID"]
client_secret = os.environ["SPOTIFY_CLIENT_SECRET"]

# コマンドライン引数の設定
parser = argparse.ArgumentParser(description='Extract Music Data from Spotify')
parser.add_argument('-p','--playlist',nargs='*', help='Spotify Playlist ID')
args = parser.parse_args()

# プレイリストIDの設定 
playlist_ids = args.playlist

print("プレイリストID: ", playlist_ids)


# Spotify APIの設定
client_credentials_manager = SpotifyClientCredentials(client_id, client_secret)
sp = spotipy.Spotify(client_credentials_manager=client_credentials_manager)

# プレイリストからトラックIDを取得する関数
def getTrackIDs(playlist_ids):
    track_ids = []

    for playlist_id in playlist_ids:
        playlist = sp.playlist(playlist_id)
        # トラック数が100以上の場合はnextが存在するので、whileで回す
        while playlist['tracks']['next']:
            for item in playlist['tracks']['items']:
                track = item['track']
                if not track['id'] in track_ids:
                    track_ids.append(track['id'])
            playlist['tracks'] = sp.next(playlist['tracks'])
        else:
            # 最後の100トラックの処理
            for item in playlist['tracks']['items']:
                track = item['track']
                if not track['id'] in track_ids:
                    track_ids.append(track['id'])

    return track_ids

# トラックIDからトラックの情報を取得する関数
def getTrackFeatures(id):
  meta = sp.track(id)
  features = sp.audio_features(id)

  name = meta['name']
  album = meta['album']['name']
  artist = meta['album']['artists'][0]['name']
  release_date = meta['album']['release_date']
  length = meta['duration_ms']
  popularity = meta['popularity']
  key = features[0]['key']
  mode = features[0]['mode']
  danceability = features[0]['danceability']
  acousticness = features[0]['acousticness']
  energy = features[0]['energy']
  instrumentalness = features[0]['instrumentalness']
  liveness = features[0]['liveness']
  loudness = features[0]['loudness']
  speechiness = features[0]['speechiness']
  tempo = features[0]['tempo']
  time_signature = features[0]['time_signature']
  valence = features[0]['valence']

  track = [name, album, artist, release_date, length, popularity, key, mode,
           danceability, acousticness, energy, instrumentalness, liveness,
           loudness, speechiness, tempo, time_signature, valence]
  return track

# 処理の実行

print("プレイリストからトラックIDを取得中")
track_ids = getTrackIDs(playlist_ids)
print("取得した曲数: ", len(track_ids))

print("各トラックの情報を取得中")
tracks = []
for track_id in track_ids:
    time.sleep(0.5)
    track = getTrackFeatures(track_id)
    tracks.append(track)

# 配列をデータフレームに変換してヘッダ情報を追加
df = pd.DataFrame(tracks, columns = ['曲名', 'アルバム', 'アーティスト', 'リリース日', '長さ', '人気度', 'キー', '曲調', 'ダンス度', 'アコースティック度', 'エネルギー', 'インスト感', 'ライブ感', '音の大きさ', 'スピーチ度', 'BPM', '拍子', '曲のポジティブ度'])
df.head()

# CSVファイルとして出力
df.to_csv('spotify_music_extract.csv', sep = ',')

